import { useState, useEffect } from 'react';

const localCache = {};

export default function useBreedList(animal) {
    const [breedList, setBreedList] = useState([]);
    const [status, setStatus] = useState("unloaded")

    useEffect(() => {
        if (!animal) {
            setBreedList([])
        }
        else if (localCache[animal]) { //If you have seen it before in local cache
            setBreedList(localCache[animal]) //Set breed list to local cache
        } else {
            requestBreedList(); //If its an animal, but its not in local cache.  
        }
        async function requestBreedList() {
            setBreedList([]);
            setStatus("loading");

            const res = await fetch(
                `http://pets-v2.dev-apis.com/breeds?animal=${animal}`
            )
            const json = await res.json();
            localCache[animal] = json.breeds || [];
            setBreedList(localCache[animal]);
            setStatus("loaded");
        }
    }, [animal]);

    return [breedList, status];
}
